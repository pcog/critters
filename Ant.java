import java.awt.Color;
import java.util.Random;

public class Ant extends Critter {
	public boolean stepsNorth;
	public int mod;
	
	public Ant (){
		this.stepsNorth = stepChooser();
		this.mod = 0;
	}

	@Override
	public Direction getMove() {
		int n = mod % 2;
		mod++;
		
		if (stepsNorth){
			if (n == 0) {
				return Direction.NORTH;
			}
			else {
				return Direction.EAST;
			}
		}
		else {
			if (n == 0) {
				return Direction.SOUTH;
			}
			else {
				return Direction.WEST;
			}
		}
	}

	public boolean stepChooser() {
		Random rand = new Random();
		int n = rand.nextInt(2);
		if (n == 0) {
			return stepsNorth = false;
		}
		else {
			return stepsNorth = true;
		}
	}
	
	@Override
	public FoodType getFoodType() {
		
		return FoodType.GRASS;
	}

	@Override
	public Color getColor() {
		
		return Color.black;
	}

	@Override
	public Speed getSpeed() {
		
		return Speed.FAST;
	}
	
	public String toString() { return "%"; }
	
}
