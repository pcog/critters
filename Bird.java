import java.awt.Color;

public class Bird extends Critter {
	public int mod;
	public Direction lastDir;
	
	public Bird (){
		this.mod = 0;
		this.lastDir = Direction.NORTH;
	}

	@Override
	public Direction getMove() {
		int n = mod % 12;
		mod++;
		
		if(n < 3) {
			lastDir = Direction.NORTH;
			return Direction.NORTH;
		}
		else if(n < 6) {
			lastDir = Direction.EAST;
			return Direction.EAST;
		}
		else if(n < 9) {
			lastDir = Direction.SOUTH;
			return Direction.SOUTH;
		}
		else {
			lastDir = Direction.WEST;
			return Direction.WEST;
		}
	}
	
	@Override
	public FoodType getFoodType() {
		return FoodType.GRASS;
	}

	@Override
	public Color getColor() {
		return Color.red;
	}

	@Override
	public Speed getSpeed() {
		return Speed.FAST;
	}
	
	@Override
	public String toString() { 
		switch (lastDir){
		case NORTH: return"∧";
		case EAST: return">";
		case SOUTH: return"V";
		case WEST: return "<";
		default:
			return null;
		}
		
	}
}
