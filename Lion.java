import java.awt.Color;
import java.util.Random;

public class Lion extends Cat{
	public int moveNum;
	public boolean first,rest;
	public int n, y;

	public Lion() {
		this.moveNum = 0;
		this.first = true;
		this.n = 0;
		this.y = 0;
	}

	@Override
	public Direction getMove() {

		int sleepNum = moveNum % 8;
		int stepNum = moveNum % 5;

		if (sleepNum == 0 && first == false && y >= 0) {
			rest = true;
			return sleeper();	
		}
		else {
			first = false;
			moveNum++;

			if (stepNum == 0) {
				rest = false;
				return stepChooser();
			}
			else {
				return lastDir;
			}
		}
	}

	public Direction sleeper() {
		if(y == 0) {
			Random rand = new Random();
			n = rand.nextInt(6);
			y = n;
			moveNum++;
		}
		y--;
		if (y == -1) {
			moveNum++;
			return lastDir;}

		lastDir = Direction.CENTER;
		return Direction.CENTER;
	}

	@Override
	public FoodType getFoodType() {
		return FoodType.MEAT;
	}

	@Override
	public Color getColor() {
		return Color.ORANGE;
	}

	@Override
	public Speed getSpeed() {
		return Speed.SLOW;
	}

	@Override
	public String toString() {
		if (rest){
			return "Z";}
			else return "L";		
		}
}
