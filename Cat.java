import java.awt.Color;
import java.util.Random;

public class Cat extends Critter {
	public int mod;
	public Direction lastDir;

	public Cat() {
		this.mod = 0;
		this.lastDir = null;
	}

	@Override
	public Direction getMove() {
		int x = mod % 5;
		mod++;

		if (x == 0) {
			return stepChooser();
		}
		else {
			return lastDir;
		}
	}
	
	public Direction stepChooser() {
		Random rand = new Random();
		int n = rand.nextInt(4); 

		switch (n){
		case 0: lastDir = Direction.NORTH; return Direction.NORTH;
		case 1: lastDir = Direction.EAST; return Direction.EAST;
		case 2: lastDir = Direction.SOUTH; return Direction.SOUTH;
		case 3: lastDir = Direction.WEST; return Direction.WEST;
		default:
			return Direction.NORTH;
		}
	}

	@Override
	public FoodType getFoodType() {
		return FoodType.MEAT;
	}

	@Override
	public Color getColor() {
		return Color.magenta;
	}

	@Override
	public Speed getSpeed() {
		return Speed.MEDIUM;
	}

	@Override
	public String toString() {
		return "c";
	}
}
