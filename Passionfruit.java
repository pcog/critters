import java.awt.Color;

public class Passionfruit extends Critter{
private int step;

	public Passionfruit() {
		this.step = 0;
	}

	@Override
	public Direction getMove() {
		step++;
		return Direction.NORTH;
	}

	@Override
	public FoodType getFoodType() {
		int mod1 = step % 2;
		if(mod1 == 0) {
			return FoodType.GRASS;
		}
		else {
			return FoodType.MEAT;
		}
	}

	@Override
	public Color getColor() {
		int mod2 = step % 5;
		switch (mod2){
		case 0: return Color.MAGENTA;
		case 1: return Color.BLUE;
		case 2: return Color.CYAN;
		case 3: return Color.GREEN;
		case 4: return Color.PINK;
		default:
			return Color.MAGENTA;
		}
	}

	@Override
	public Speed getSpeed() {
		return Speed.FAST;
	}
	
	@Override
	public String toString() {
		return "⛀";
	}
}
